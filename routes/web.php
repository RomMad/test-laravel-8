<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AppController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\NewsletterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/', function () {
    return view('app/home');
})->name('home');

// Route::get('/home', function () {
    //     return view('app/home');
    // })->name('home');

Route::get('/home', [AppController::class, 'home'])->name('home');
// Route::get('/home/{name?}', [AppController::class, 'home'])->name('home_name');

Route::get('/movie/new', [MovieController::class, 'newMovie'])->name('movie_new')->middleware('auth');
Route::post('/movie/create', [MovieController::class, 'createMovie'])->name('movie_create')->middleware('auth');
Route::get('/movie/{id}/edit', [MovieController::class, 'editMovie'])->name('movie_edit')->middleware('auth');
Route::get('/movie/{id}/show', [MovieController::class, 'showMovie'])->name('movie_show');
Route::get('/movies', [MovieController::class, 'showMovies'])->name('movies');

Route::get('/event/{id}', [EventController::class, 'showEvent'])->name('event');
Route::get('/events', [EventController::class, 'showEvents'])->name('events');

Route::get('/admin', [AdminController::class, 'admin'])->name('admin');

Route::get('/newletter/send', [NewsletterController::class, 'sendNewsletter'])->name('newsletter_send');

Route::get('/contact', fn () => view('app/contact'))->name('contact');

Route::get('test-json', fn () => response()->json([
    'title' => 'Ceci est un titre',
    'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. 
        Corporis consequuntur laboriosam minus? Temporibus minima, eligendi quae debitis aliquid accusantium quia reprehenderit, 
        non similique provident deserunt iure fuga. Magnam, voluptas asperiores',
]));
