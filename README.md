# Test Laravel 8

## Commands
### Run the server:
```bash
php artisan serve
```

### Show all the commands:
```bash
php artisan list
```

### Cache the config:
```bash
php artisan config:cache
```
If you have this message "Failed to clear cache. Make sure you have the appropriate permissions.", do this command before:
```bash
php artisan config:clear
```
### Make a Model:
```bash
php artisan make:controller <ModelName>
```
Or with migration option:
```bash
php artisan make:controller <ModelName> --migration (-m)
```

### Make a controller:
```bash
php artisan make:controller <ControllerName>
```

### Create a database:
No command for that. So you can create the database with SQL query or with PhpMyAdmin.

And make migration:
```bash
php artisan migrate
```

### Make Factories (fixtures)
```bash
php artisan make:factory <FactoryClassName> --model=ModelClassName
```

### Play factories
```bash
php artisan tinker
```
```bash
<ModelClassName>::factory()->count(x)->create();
```

### Authentification
https://laravel.com/docs/8.x/authentication
Install the breeze component
```bash
composer require laravel/breeze --dev
```
```bash
php artisan breeze:install
```


### Mail
```bash
php artisan make:mail <ClassName>
```
Run mailhog:
```bash
docker run -d -p 1025:1025 -p 8025:8025 mailhog/mailhog
```



