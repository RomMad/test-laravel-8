<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Movie extends Model
{
    use HasFactory;

    /** @var array */
    protected $guarded = [
        'id',
    ];

    public function run(): void
    {
        Movie::factory()
            ->count(50)
            ->create();
    }

    public function comments()
    {
        // return $this->hasMany(Comment::class);
        return $this->morphMany(PolyComment::class, 'commentable');
    }

    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }
}
