<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    public function run(): void
    {
        Comment::factory()
            ->count(50)
            ->create();
    }

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }
}
