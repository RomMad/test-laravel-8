<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    public function run(): void
    {
        Movie::factory()
            ->count(50)
            ->create();
    }

    public function comments()
    {
        return $this->morphMany(PolyComment::class, 'commentable');
    }
}
