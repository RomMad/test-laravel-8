<?php

namespace App\Http\Controllers;

use App\Mail\NewsletterMail;
use Illuminate\Support\Facades\Mail;

class NewsletterController extends Controller
{
    public function sendNewsletter()
    {
        Mail::to('xxx@xxx.fr')->send(new NewsletterMail());

        return view('app/home');
    }
}
