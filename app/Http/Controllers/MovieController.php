<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use App\Models\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function showMovie(int $id)
    {
        return view('app/movie/movie', [
            'movie' => Movie::find($id),
        ]);
    }

    public function showMovies()
    {
        $movies = Movie::all();

        return view('app/movie/listMovies', compact('movies'));
    }

    public function newMovie()
    {
        return view('app/movie/formMovie', [
            'movie' => null,
            'genres' => Genre::all(),
        ]);
    }

    public function editMovie(int $id)
    {
        return view('app/movie/formMovie', [
            'movie' => Movie::find($id),
            'genres' => Genre::all() ?? null,
        ]);
    }

    public function createMovie(Request $request)
    {
        $this->validate($request, [
            'title' => ['required', 'min:2'],
            'duration' => ['required'],
        ]);

        $movie = Movie::create([
            'title' => $request->title,
            'duration' => $request->duration,
            'plot' => $request->plot,
        ]);

        foreach ($request->genres as $genreId) {
            $movie->genres()->attach($genreId);
        }

        return view('app/movie/movie', [
            'movie' => $movie,
        ]);
    }
}
