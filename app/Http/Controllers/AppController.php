<?php

namespace App\Http\Controllers;

class AppController extends Controller
{
    public function home(string $name = null)
    {
        return view('app/home', [
            'name' => $name,
        ]);
    }
}
