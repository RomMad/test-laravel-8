<?php

namespace App\Http\Controllers;

use App\Models\Event;

class EventController extends Controller
{
    public function showEvent(int $id)
    {
        return view('app/event/event', [
            'event' => Event::find($id),
        ]);
    }

    public function showEvents()
    {
        $events = Event::all();

        return view('app/event/listEvents', compact('events'));
    }
}
