<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Gate;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function admin()
    {
        if (!Gate::allows('admin')) {
            abort(403);
        }

        return view('app/admin/admin');
    }
}
