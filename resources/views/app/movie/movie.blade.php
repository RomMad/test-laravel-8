@extends('base')

@section('title', $movie->title) 

@section('body')
    <div class="my-5">
        <h1 class="mb-4">{{ $movie->title }}</h1>
        <div>
            @foreach ($movie->genres as $genre)
                <span class="badge bg-info">{{ $genre->name }}</span>
            @endforeach
        </div>
        <hr>
        <h2 class="h3">Synopis</h2>
        <p>{{ $movie->plot }}</p>

        <div class="row">
            <div>
                <a class="btn btn-primary" href="{{ route('movie_edit', ['id' => $movie->id]) }}">
                    Editer
                </a>
            </div>
        </div>
        <hr>
        <h2 class="h3">Avis</h2>
        @forelse ($movie->comments as $comment)
        <div>
            <h3 class="h5">{{ $comment->title }}</h3>
            <p class="text-secondary">{{ $comment->content }}</p>
        </div>
            
        @empty
            <div><p>Aucun avis pour le moment.</p></div>
        @endforelse

    </div>
@endsection