@extends('base')

@section('title', 'Mes Films') 

@section('body')
    <div class="my-5">
        <h1 class="mb-4">Mes films</h1>
        <hr>
        @auth
            <div class="row mb-3">
                <a href="{{ route('movie_new') }}" class="mb-3 link-dark">Ajouter un nouveau film</a>
            </div>
        @endauth
        @foreach ($movies as $id => $movie)
            <div class="row mb-3">
                <div class="col-md-12">
                    <p> <a href="{{ route('movie_show', $movie->id) }}">{{ $movie->title }}</a> ({{ $movie->duration }} mn)</p>
                </div>
            </div>
        @endforeach
    </div>
@endsection