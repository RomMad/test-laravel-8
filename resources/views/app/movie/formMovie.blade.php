@extends("base")

@section("title", $title = $movie ? $movie->title : "Nouveau film") 

@section("body")
    <div class="my-5">
        <div class="row">
            <h1 class="mb-4">{{ $title }}</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('movie_create') }}" method="post">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <label for="title" class="form-label">Titre du film</label>
                            <input type="text" name="title" id="title" class="form-control
                                @error('title') is-invalid @enderror"
                                value="{{ $movie?->title ?? null }}">
                            @error('title')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label for="duration" class="form-label">Durée</label>
                            <input type="number" name="duration" id="duration" class="form-control
                                @error('duration') is-invalid @enderror"
                                value="{{ $movie?->duration ?? null }}">
                            @error('duration')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            @if (count($genres))
                                <label for="genres" class="form-label">Genre(s)</label>
                                <select class="form-select" name="genres[]" multiple="multiple" size="3" aria-label="multiple select">
                                    @foreach ($genres as $genre)
                                        <option value="{{ $genre->id }}">{{ $genre->name }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <label for="plot" class="form-label">Résumé</label>
                            <textarea class="form-control" name="plot" id="plot" rows="3">{{ $movie?->plot ?? null }}</textarea>
                        </div>  
                    @csrf
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <button type="submit" name="send" class="btn btn-primary">Valider</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection