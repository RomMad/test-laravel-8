@extends('base')

@section('title', 'Accueil')

@section('body')
    <div class="my-5">
        <h1 class="mb-4">Accueil | Projet test Laraval 8</h1>
        <div class="row">
            <div class="col-md-12">
                <p>Bonjour {{ Auth::user()->name ?? '' }} !</p>
                <p>Bienvenue sur la page d'accueil de mon site ! :)</p>
                <p>Nom du site : {{ config('app.name') }}</p>
                <p>URL : {{ config('app.url') }}</p>
                <p>Base de données : {{ config('database.connections.mysql.driver') }}</p>
            </div>
        </div>
    </div>
@endsection