@extends('base')

@section('title', $event->name) 

@section('body')
    <div class="my-5">
        <h1 class="mb-4">{{ $event->name }}</h1>
        {{-- <div>
            @foreach ($event->genres as $genre)
                <span class="badge bg-info">{{ $genre->name }}</span>
            @endforeach
        </div> --}}
        <hr>
        <h2 class="h3">Description</h2>
        <p>{{ $event->description }}</p>
        <hr>
        <h2 class="h3">Commentaires</h2>
        @forelse ($event->comments as $comment)
        <div>
            <h3 class="h5">{{ $comment->title }}</h3>
            <p class="text-secondary">{{ $comment->content }}</p>
        </div>
            
        @empty
            <div><p>Aucun commentaire pour le moment.</p></div>
        @endforelse

    </div>
@endsection²