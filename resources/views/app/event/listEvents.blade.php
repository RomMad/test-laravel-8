@extends('base')

@section('title', 'Evénements') 

@section('body')
    <div class="my-5">
        <h1 class="mb-4">Evénements</h1>
        @foreach ($events as $id => $event)
            <div class="row mb-3">
                <div class="col-md-12">
                    <p> <a href="{{ route('event', $event->id) }}">{{ $event->name }}</a></p>
                </div>
            </div>
        @endforeach
    </div>
@endsection