<div class="bg-dark text-light ">
    <div class="p-4">
        <p class="m-0">&copy; Copyright {{ date('Y')  }} | Romain Madelaine</p>
    </div>
</div>