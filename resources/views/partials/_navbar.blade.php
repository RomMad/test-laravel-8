<div class="container">
    <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="{{ route('home') }}" class="d-flex align-items-center mb-2 mb-lg-0 text-dark text-decoration-none">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-film" viewBox="0 0 16 16">
                <path d="M0 1a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v14a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V1zm4 0v6h8V1H4zm8 8H4v6h8V9zM1 1v2h2V1H1zm2 3H1v2h2V4zM1 7v2h2V7H1zm2 3H1v2h2v-2zm-2 3v2h2v-2H1zM15 1h-2v2h2V1zm-2 3v2h2V4h-2zm2 3h-2v2h2V7zm-2 3v2h2v-2h-2zm2 3h-2v2h2v-2z"/>
            </svg>
        </a>
        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
            <li><a href="{{ route('home') }}" class="nav-link px-2 link-secondary">Accueil</a></li>
            <li><a href="{{ route('movies') }}" class="nav-link px-2 link-dark">Mes films</a></li>
            <li><a href="{{ route('events') }}" class="nav-link px-2 link-dark">Evénements</a></li>
            @if (Auth::user() && Auth::user()->can('admin'))
                <li><a href="{{ route('admin') }}" class="nav-link px-2 link-dark">Admin</a></li>
            @endif
            <li><a href="{{ route('contact') }}" class="nav-link px-2 link-dark">Contact</a></li>
            <li>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#">Musique</a></li>
                    <li><a class="dropdown-item" href="#">Cinéma</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="#">Sport</a></li>
                </ul>
            </li>
        </ul>
        <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
            <input type="search" class="form-control" placeholder="Rercherche..." aria-label="Search">
        </form>
        @auth
            <div class="dropdown text-end">
                <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                    <img src="{{ url('images/romain-min.jpg') }}" alt="Photo Romain" width="32" height="32" class="rounded-circle">
                </a>
                <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
                    <li><a class="dropdown-item" href="{{ route('movie_new') }}">Nouveau film</a></li>
                    <li><a class="dropdown-item" href="{{ route('dashboard') }}">Tableau de bord</a></li>
                    <li><a class="dropdown-item" href="#">Profil</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="{{ route('logout') }}">Déconnexion</a></li>
                </ul>
            </div>
        @else
            <ul class="nav flex-column">
                <li class="nav-item"><a href="{{ route('register') }}" class="nav-link px-2 py-0 link-dark">Créer un compte</a></li>
                <li class="nav-item"><a href="{{ route('login') }}" class="nav-link px-2 py-0 link-dark">Se connecter</a></li>
            </ul>
        @endauth
    </div>
</div>