<?php

namespace Database\Factories;

use App\Models\Event;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(4),
            'description' => join('', $this->faker->paragraphs(4)),
            'startDate' => $startDate = $this->faker->dateTimeBetween('-1 year', '+6 months'),
            'endDate' => $this->faker->dateTimeBetween($startDate, (clone $startDate)->modify('+1 week')),
        ];
    }
}
